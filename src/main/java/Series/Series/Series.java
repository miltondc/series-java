package Series.Series;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import jxl.write.DateTime;

public class Series implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public DateTime fechaHora;
	public Date fecha;
	public Time hora;
	public float valorInicio;
	public float valorMayor;
	public float valorMenor;
	public float valorFin;
	
	
	public DateTime getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(DateTime fechaHora) {
		this.fechaHora = fechaHora;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Time getHora() {
		return hora;
	}
	public void setHora(Time hora) {
		this.hora = hora;
	}
	public float getValorInicio() {
		return valorInicio;
	}
	public void setValorInicio(float valorInicio) {
		this.valorInicio = valorInicio;
	}
	public float getValorMayor() {
		return valorMayor;
	}
	public void setValorMayor(float valorMayor) {
		this.valorMayor = valorMayor;
	}
	public float getValorMenor() {
		return valorMenor;
	}
	public void setValorMenor(float valorMenor) {
		this.valorMenor = valorMenor;
	}
	public float getValorFin() {
		return valorFin;
	}
	public void setValorFin(float valorFin) {
		this.valorFin = valorFin;
	}
	
	
	

}
