package Series.Series;

import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import javax.swing.JButton;
import javax.swing.JTextField;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.TimeZone;

//import jxl.write.DateTime;
import javax.swing.JLabel;

import java.awt.Cursor;
import java.awt.Font;
import javax.swing.border.EtchedBorder;

import org.springframework.beans.factory.support.ReplaceOverride;

public class PanelPrincipal extends JPanel{

	int totalPeriodosValidos = 0;
	int totalPeriodosVolatilidad = 0;
	int totalVecesPorcentajeMayor = 0;//dentro del periodo
	int totalVecesPorcentajeMenor = 0;//dentro del periodo

	int totalVecesPorcentajeMayorAFechaFin = 0;
	int totalVecesPorcentajeMenorAFechaFin = 0;

	float variacionMayor = 0;
	float variacionMenor = 0;

	Date fechaComienzo;//Rango de la consulta(desde-hasta)
	Date fechaFinalizacion;//hasta

	JScrollPane panelSimple;
	DefaultTableModel modeloSimple;
	JTable tablaSimple;

	ArrayList<Float> listaVariaciones;
	ArrayList<Rangos> listaRangos2;
	ArrayList<String> listaRangos;

	JScrollPane panelVolatilidad;
	DefaultTableModel modeloVolatilidad;
	JTable tablaVolatilidad;

	private JTextField txtRutaArchivo;
	UtilDateModel model;
	UtilDateModel model2;
	JDatePanelImpl datePanelInicio;
	JDatePanelImpl datePanelFin;
	JDatePickerImpl datePickerInicio;
	JDatePickerImpl datePickerFin;
	JLabel lblTxt1;
	JLabel lblTxt2;
	JButton btnExaminarVolatilidad;
	JButton btnExaminar;
	MaskFormatter mascara;

	float valorInicio;
	float valorFin;
	float variacion;


	public static SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd HHmmss");
	public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	public static SimpleDateFormat sdfHora = new SimpleDateFormat("hh:MM:ss");
	public static SimpleDateFormat sdfFecha = new SimpleDateFormat("yyyyMMdd");

	private ArrayList<Series> listaSimple;
	private ArrayList<Series> listaVolatilidad;
	private ArrayList<Series> listaPeriodoValido;
	public String cadena = new String();
	String nombreArchivo;
	private JTextField txtRutaVolatilidad;
	private JTextField txtPorcentaje;
	private JTextField txtDias;
	private JButton btnCalcular;
	private JTextField txtHoraInicio;
	private JTextField txtHoraFin;
	private JTextField txtMayorQue;
	private JTextField txtMenorQue;

	public PanelPrincipal() {
		setLayout(null);

		panelSimple = new JScrollPane();
		panelSimple.setBounds(24, 49, 403, 236);
		add(panelSimple);

		model = new UtilDateModel();
		datePanelInicio = new JDatePanelImpl(model);
		model2 = new UtilDateModel();
		datePanelFin = new JDatePanelImpl(model2);

		modeloSimple = new DefaultTableModel();
		modeloSimple.addColumn("FECHA");
		modeloSimple.addColumn("HORA");
		modeloSimple.addColumn("INICIO");
		modeloSimple.addColumn("MAYOR");
		modeloSimple.addColumn("MENOR");
		modeloSimple.addColumn("FIN");
		modeloSimple.addColumn("");

		tablaSimple = new JTable(modeloSimple){
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};

		tablaSimple.getColumnModel().getColumn(0).setMaxWidth(80);
		tablaSimple.getColumnModel().getColumn(0).setMinWidth(80);
		tablaSimple.getColumnModel().getColumn(0).setPreferredWidth(80);

		tablaSimple.getColumnModel().getColumn(1).setMaxWidth(80);
		tablaSimple.getColumnModel().getColumn(1).setMinWidth(80);
		tablaSimple.getColumnModel().getColumn(1).setPreferredWidth(80);

		tablaSimple.getColumnModel().getColumn(2).setMaxWidth(60);
		tablaSimple.getColumnModel().getColumn(2).setMinWidth(60);
		tablaSimple.getColumnModel().getColumn(2).setPreferredWidth(60);

		tablaSimple.getColumnModel().getColumn(3).setMaxWidth(60);
		tablaSimple.getColumnModel().getColumn(3).setMinWidth(60);
		tablaSimple.getColumnModel().getColumn(3).setPreferredWidth(60);

		tablaSimple.getColumnModel().getColumn(4).setMaxWidth(60);
		tablaSimple.getColumnModel().getColumn(4).setMinWidth(60);
		tablaSimple.getColumnModel().getColumn(4).setPreferredWidth(60);

		tablaSimple.getColumnModel().getColumn(5).setMaxWidth(60);
		tablaSimple.getColumnModel().getColumn(5).setMinWidth(60);
		tablaSimple.getColumnModel().getColumn(5).setPreferredWidth(60);

		tablaSimple.getColumnModel().getColumn(6).setMaxWidth(60);
		tablaSimple.getColumnModel().getColumn(6).setMinWidth(60);
		tablaSimple.getColumnModel().getColumn(6).setPreferredWidth(60);

		panelSimple.add(tablaSimple);
		panelSimple.setViewportView(tablaSimple);
		add(panelSimple);

		panelVolatilidad = new JScrollPane();
		panelVolatilidad.setBounds(437, 49, 403, 236);
		add(panelVolatilidad);

		modeloVolatilidad = new DefaultTableModel();
		modeloVolatilidad.addColumn("FECHA");
		modeloVolatilidad.addColumn("HORA");
		modeloVolatilidad.addColumn("INICIO");
		modeloVolatilidad.addColumn("MAYOR");
		modeloVolatilidad.addColumn("MENOR");
		modeloVolatilidad.addColumn("FIN");
		modeloVolatilidad.addColumn("");

		tablaVolatilidad = new JTable(modeloVolatilidad){
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};

		tablaVolatilidad.getColumnModel().getColumn(0).setMaxWidth(80);
		tablaVolatilidad.getColumnModel().getColumn(0).setMinWidth(80);
		tablaVolatilidad.getColumnModel().getColumn(0).setPreferredWidth(80);

		tablaVolatilidad.getColumnModel().getColumn(1).setMaxWidth(80);
		tablaVolatilidad.getColumnModel().getColumn(1).setMinWidth(80);
		tablaVolatilidad.getColumnModel().getColumn(1).setPreferredWidth(80);

		tablaVolatilidad.getColumnModel().getColumn(2).setMaxWidth(60);
		tablaVolatilidad.getColumnModel().getColumn(2).setMinWidth(60);
		tablaVolatilidad.getColumnModel().getColumn(2).setPreferredWidth(60);

		tablaVolatilidad.getColumnModel().getColumn(3).setMaxWidth(60);
		tablaVolatilidad.getColumnModel().getColumn(3).setMinWidth(60);
		tablaVolatilidad.getColumnModel().getColumn(3).setPreferredWidth(60);

		tablaVolatilidad.getColumnModel().getColumn(4).setMaxWidth(60);
		tablaVolatilidad.getColumnModel().getColumn(4).setMinWidth(60);
		tablaVolatilidad.getColumnModel().getColumn(4).setPreferredWidth(60);

		tablaVolatilidad.getColumnModel().getColumn(5).setMaxWidth(60);
		tablaVolatilidad.getColumnModel().getColumn(5).setMinWidth(60);
		tablaVolatilidad.getColumnModel().getColumn(5).setPreferredWidth(60);

		tablaVolatilidad.getColumnModel().getColumn(6).setMaxWidth(60);
		tablaVolatilidad.getColumnModel().getColumn(6).setMinWidth(60);
		tablaVolatilidad.getColumnModel().getColumn(6).setPreferredWidth(60);

		panelVolatilidad.add(tablaVolatilidad);
		panelVolatilidad.setViewportView(tablaVolatilidad);
		add(panelVolatilidad);



		btnExaminar = new JButton("...");
		btnExaminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				FileNameExtensionFilter filtro = new FileNameExtensionFilter(".txt", "txt");
				fc.setFileFilter(filtro);
				int respuesta = fc.showOpenDialog(null);

				if (respuesta == JFileChooser.APPROVE_OPTION){

					cadena = fc.getSelectedFile().getAbsolutePath();
					nombreArchivo = fc.getSelectedFile().getName();
					txtRutaArchivo.setText(cadena);
					txtRutaArchivo.setEditable(false);
					setTxtSimple(cadena);
					lblTxt1.setText("1 - "+nombreArchivo+" ("+listaSimple.size()+" Registros)");

				}
			}
		});
		btnExaminar.setBounds(311, 296, 24, 23);
		add(btnExaminar);

		txtRutaArchivo = new JTextField();
		txtRutaArchivo.setBounds(55, 296, 246, 31);
		add(txtRutaArchivo);
		txtRutaArchivo.setColumns(10);

		lblTxt1 = new JLabel("");
		lblTxt1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTxt1.setBounds(24, 11, 403, 27);
		add(lblTxt1);

		txtRutaVolatilidad = new JTextField();
		txtRutaVolatilidad.setBounds(457, 296, 246, 31);
		add(txtRutaVolatilidad);
		txtRutaVolatilidad.setColumns(10);

		btnExaminarVolatilidad = new JButton("...");
		btnExaminarVolatilidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				FileNameExtensionFilter filtro = new FileNameExtensionFilter(".txt", "txt");
				fc.setFileFilter(filtro);
				int respuesta = fc.showOpenDialog(null);

				if (respuesta == JFileChooser.APPROVE_OPTION){

					cadena = fc.getSelectedFile().getAbsolutePath();
					nombreArchivo = fc.getSelectedFile().getName();
					txtRutaVolatilidad.setText(cadena);
					txtRutaVolatilidad.setEditable(false);
					setTxtVolatilidad(cadena);
					lblTxt2.setText("2 - "+nombreArchivo+" ("+listaVolatilidad.size()+" Registros)");

				}
			}
		});
		btnExaminarVolatilidad.setBounds(713, 296, 24, 23);
		add(btnExaminarVolatilidad);

		lblTxt2 = new JLabel("");
		lblTxt2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTxt2.setBounds(437, 11, 403, 27);
		add(lblTxt2);

		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(59, 357, 746, 250);
		add(panel);
		panel.setLayout(null);
		datePickerInicio = new JDatePickerImpl(datePanelInicio);
		datePickerInicio.setBounds(91, 17, 182, 31);
		panel.add(datePickerInicio);


		datePickerFin = new JDatePickerImpl(datePanelFin);
		datePickerFin.setBounds(407, 17, 182, 31);
		panel.add(datePickerFin);

		txtPorcentaje = new JTextField();
		txtPorcentaje.setBounds(323, 114, 86, 25);
		panel.add(txtPorcentaje);
		txtPorcentaje.setColumns(10);

		txtDias = new JTextField();
		txtDias.setBounds(91, 114, 86, 25);
		panel.add(txtDias);
		txtDias.setColumns(10);


		JLabel lblFecha = new JLabel("Fecha Inicio");
		lblFecha.setBounds(10, 28, 71, 14);
		panel.add(lblFecha);

		JLabel lblDias = new JLabel("Dias");
		lblDias.setBounds(48, 122, 46, 14);
		panel.add(lblDias);

		JLabel lblPorcentaje = new JLabel("Porcentaje");
		lblPorcentaje.setBounds(254, 121, 70, 17);
		panel.add(lblPorcentaje);

		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				fechaComienzo = (Date) datePickerInicio.getModel().getValue();
				fechaFinalizacion = (Date) datePickerFin.getModel().getValue();

				if(fechaComienzo.compareTo(fechaFinalizacion) > 0){
					JOptionPane.showMessageDialog(getParent(),"La Fecha inicio debe ser menor a la Fecha fin");
				}else
					if(txtDias.getText().equals("") || datePickerInicio.getModel().getValue() == null ||
					datePanelFin.getModel().getValue() == null || txtPorcentaje.getText().equals("") ||
					txtHoraInicio.getText().equals("") || txtHoraFin.getText().equals("")){

						JOptionPane.showMessageDialog(getParent(),"Faltan Datos");
					}


					else {

						Calcular(Integer.valueOf(txtDias.getText()));
					}

			}
		});
		btnCalcular.setBounds(604, 150, 121, 74);
		panel.add(btnCalcular);

		try {
			mascara = new MaskFormatter("##:##");
			txtHoraInicio = new JFormattedTextField(mascara);
			txtHoraFin = new JFormattedTextField(mascara);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		txtHoraInicio.setBounds(90, 59, 52, 25);
		panel.add(txtHoraInicio);
		txtHoraInicio.setColumns(10);

		JLabel lblHoraInicio = new JLabel("Hora Inicio");
		lblHoraInicio.setBounds(13, 64, 67, 14);
		panel.add(lblHoraInicio);

		JLabel lblFechaFin = new JLabel("Fecha Fin");
		lblFechaFin.setBounds(323, 28, 74, 14);
		panel.add(lblFechaFin);

		JLabel lblHoraFin = new JLabel("Hora Fin");
		lblHoraFin.setBounds(323, 74, 74, 14);
		panel.add(lblHoraFin);


		txtHoraFin.setBounds(406, 69, 63, 25);
		panel.add(txtHoraFin);
		txtHoraFin.setColumns(10);

		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarDatos();
			}
		});
		btnLimpiar.setBounds(604, 118, 121, 23);
		panel.add(btnLimpiar);

		JLabel lblMayorQue = new JLabel("Mayor que");
		lblMayorQue.setBounds(48, 177, 71, 17);
		panel.add(lblMayorQue);

		txtMayorQue = new JTextField();
		txtMayorQue.setBounds(110, 177, 86, 25);
		panel.add(txtMayorQue);
		txtMayorQue.setColumns(10);

		txtMayorQue.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if (!((Character.isDigit(c) ||
						(c == KeyEvent.VK_BACK_SPACE) ||
						(c == KeyEvent.VK_DELETE)))){
					getToolkit().beep();
					e.consume();
				}
			}
		});

		JLabel lblMenorQue = new JLabel("Menor que");
		lblMenorQue.setBounds(242, 180, 71, 17);
		panel.add(lblMenorQue);

		txtMenorQue = new JTextField();
		txtMenorQue.setBounds(323, 177, 86, 25);
		panel.add(txtMenorQue);
		txtMenorQue.setColumns(10);

		txtMenorQue.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if (!((Character.isDigit(c) ||
						(c == KeyEvent.VK_BACK_SPACE) ||
						(c == KeyEvent.VK_DELETE)))){
					getToolkit().beep();
					e.consume();
				}
			}
		});

		txtDias.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {//EVENTO PARA QUE SOLO ACEPTE NUMEROS
				char c = e.getKeyChar();
				if (!((Character.isDigit(c) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE)))){
					getToolkit().beep();
					e.consume();
				}
			}
		});

		txtPorcentaje.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if(((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE) && (c !='.')){


					getToolkit().beep();
					e.consume();
				}
			}
		});

	}


	public void setTxtSimple(String c){//se carga el arraylist a partir de txt
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		StringTokenizer lineaTxt;
		String linea;
		Object [] s = new Object[5];
		Series series;
		listaSimple = new ArrayList<Series>();

		try{
			FileReader fr = new FileReader(c);
			BufferedReader br = new BufferedReader(fr);

			try{

				while((linea = br.readLine()) != null){

					series = new Series();
					lineaTxt = new StringTokenizer(linea,";");
					series.setFecha(esVerano(formato.parse(lineaTxt.nextToken())));
					series.setValorInicio(Float.parseFloat(lineaTxt.nextToken()));
					series.setValorMayor(Float.parseFloat(lineaTxt.nextToken()));
					series.setValorMenor(Float.parseFloat(lineaTxt.nextToken()));
					series.setValorFin(Float.parseFloat(lineaTxt.nextToken()));

					if(horaValida(series.getFecha()))listaSimple.add(series);

				}
			}

			catch(Exception e){
				e.printStackTrace();
			}

			setTxtSimple(listaSimple);

		}catch(Exception e){
			e.printStackTrace();
		}
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	public void setTxtVolatilidad(String c){

		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		StringTokenizer lineaTxt;
		String linea;
		Object [] s = new Object[5];
		Series series;
		listaVolatilidad = new ArrayList<Series>();

		try{
			FileReader fr = new FileReader(c);
			BufferedReader br = new BufferedReader(fr);

			try{

				while((linea = br.readLine()) != null){

					series = new Series();
					lineaTxt = new StringTokenizer(linea,";");
					series.setFecha(esVerano(formato.parse(lineaTxt.nextToken())));
					series.setValorInicio(Float.parseFloat(lineaTxt.nextToken()));
					series.setValorMayor(Float.parseFloat(lineaTxt.nextToken()));
					series.setValorMenor(Float.parseFloat(lineaTxt.nextToken()));
					series.setValorFin(Float.parseFloat(lineaTxt.nextToken()));

					if(horaValida(series.getFecha()))listaVolatilidad.add(series);

				}
			}

			catch(Exception e){
				e.printStackTrace();
			}



			setTxtVolatilidad(listaVolatilidad);

		}catch(Exception e){
			e.printStackTrace();
		}

		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	public void setTxtSimple(ArrayList<Series> l){

		limpiarTabla(tablaSimple);
		Time hora;

		for(Series s : l){

			Object [] ser = new Object[6];

			hora = new Time(s.getFecha().getTime());

			ser[0] = sdf.format(s.getFecha());
			ser[1] = hora;
			ser[2] = s.getValorInicio();
			ser[3] = s.getValorMayor();
			ser[4] = s.getValorMenor();
			ser[5] = s.getValorFin();

			modeloSimple.addRow(ser);
		}
	}

	public void setTxtVolatilidad(ArrayList<Series> l){

		limpiarTabla(tablaVolatilidad);
		Time hora;

		for(Series s : l){

			Object [] ser = new Object[6];

			hora = new Time(s.getFecha().getTime());

			ser[0] = sdf.format(s.getFecha());
			ser[1] = hora;
			ser[2] = s.getValorInicio();
			ser[3] = s.getValorMayor();
			ser[4] = s.getValorMenor();
			ser[5] = s.getValorFin();

			modeloVolatilidad.addRow(ser);
		}
	}

	public void limpiarTabla(JTable tabla){
		try {
			DefaultTableModel modelo=(DefaultTableModel) tabla.getModel();
			int filas = tabla.getRowCount();
			for (int i = 0;filas>i; i++) {
				modelo.removeRow(0);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
		}
	}

	public void Calcular(int dias){

		totalPeriodosValidos = 0;
		totalPeriodosVolatilidad = 0;
		totalVecesPorcentajeMayor = 0;
		totalVecesPorcentajeMenor = 0;
		totalVecesPorcentajeMayorAFechaFin = 0;
		totalVecesPorcentajeMenorAFechaFin = 0;
		listaVariaciones = new ArrayList<Float>();

		Date fechaInicioPeriodo;//fecha inicio de cada periodo a evaluar
		Date fechaFinPeriodo;//fecha fin de cada periodo a evaluar

		String horaInicio = txtHoraInicio.getText();	
		String horaFin = txtHoraFin.getText();

		fechaComienzo = insertarHora(fechaComienzo, horaInicio.replaceAll(":", ""));//toma la fecha y hora ingresada y las concatena
		fechaFinalizacion = insertarHora(fechaFinalizacion,horaFin.replaceAll(":", ""));//si no se carga la hora en un Date, por defecto toma la del sistema


		//===================Hasta aca se preparan los datos antes a analizar=============================


		//for(int i = Integer.valueOf(sdfFecha.format(fechaComienzo)); i <= Integer.valueOf(sdfFecha.format(fechaFinalizacion)); i++){


		while(fechaComienzo.compareTo(fechaFinalizacion) < 0){
			
			fechaInicioPeriodo = fechaComienzo;
			fechaFinPeriodo = insertarHora(sumarDias(fechaComienzo, dias),horaFin.replaceAll(":", ""));

			if(existePeriodo(fechaComienzo, fechaFinPeriodo)){

				totalPeriodosValidos += 1;
				calcularVariaciones(listaPeriodoValido);

			}
			if(existePeriodoVolatilidad(fechaComienzo, fechaFinPeriodo)){

				totalPeriodosVolatilidad += 1;

			}

			fechaComienzo = sumarDias(fechaComienzo, 7);//fechaFinPeriodo;//la fecha fin periodo se la asigna al inicio asi continua la busqueda desde el fin del periodo
			//fechaComienzo = insertarHora(fechaComienzo, horaInicio.replaceAll(":", ""));// se le vuelva a insertar la hora sino queda con la hora del fin


		}

		armarArreglo(variacionMenor, variacionMayor);
		VentanaResultado v = new VentanaResultado(txtPorcentaje.getText());
		v.completarDatos(listaRangos2,totalPeriodosValidos,totalPeriodosVolatilidad,totalVecesPorcentajeMayor,totalVecesPorcentajeMenor,
				totalVecesPorcentajeMayorAFechaFin,totalVecesPorcentajeMenorAFechaFin);
		v.setVisible(true);

		System.out.println("PERIODOS VALIDOS "+totalPeriodosValidos);
		System.out.println("PERIODOS VALIDOS VOL "+totalPeriodosVolatilidad);
		System.out.println("TOTAL VECES % MAYOR QUE "+txtPorcentaje.getText() + " = "+totalVecesPorcentajeMayor);
		System.out.println("TOTAL VECES % MENOR QUE "+txtPorcentaje.getText() + " = "+totalVecesPorcentajeMenor);
		System.out.println("TOTAL VECES % MENOR A FECHA FIN "+totalVecesPorcentajeMenorAFechaFin);
		System.out.println("TOTAL VECES % MAYOR A FECHA FIN "+totalVecesPorcentajeMayorAFechaFin);




	}

	/*public static boolean EsLunes(Date d){

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(d);

		if(cal.get(Calendar.DAY_OF_WEEK) == 2){
			return true;
		}
		else

			return false;	

	}*/

	public Date insertarHora(Date fecha, String hora){

		String h = hora.substring(0,2);
		String m = hora.substring(2, 4);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.set(Calendar.HOUR_OF_DAY,Integer.valueOf(h));
		calendar.set(Calendar.MINUTE,Integer.valueOf(m));
		calendar.set(Calendar.SECOND,0);

		return (Date) calendar.getTime();
	}

	public Date sumarDias(Date fecha, int dias){

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir
		return (Date) calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos

	}

	public Date sumarHoras(Date fecha, int horas){

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); 
		calendar.add(Calendar.HOUR, horas);  
		return (Date) calendar.getTime(); 

	}


	public boolean horaValida(Date fecha){

		boolean valido = true;

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); 

		if(calendar.get(Calendar.HOUR_OF_DAY) >= 20){
			valido = false;
		}

		else
			if(calendar.get(Calendar.HOUR_OF_DAY) < 13){
				valido = false;
			}
			else if(calendar.get(Calendar.HOUR_OF_DAY) == 13 && calendar.get(Calendar.MINUTE) < 31){
				valido = false;
			}


		return valido;

	}

	public Date esVerano(Date fecha){

		//System.out.println("ZONA "+ TimeZone.getTimeZone("Europe/Madrid").inDaylightTime(fecha));

		if(TimeZone.getTimeZone("Europe/Madrid").inDaylightTime(fecha)){
			return sumarHoras(fecha,2);
		}
		else return sumarHoras(fecha,1);

	}



	public boolean existePeriodo(Date fechaI, Date fechaF){

		//System.out.println("FECHA INICIO "+fechaI);
		//System.out.println("FECHA FIN "+fechaF);
		boolean existeInicio = false;
		boolean existeFin = false;
		int i;
		listaPeriodoValido = new ArrayList<Series>();

		//System.out.println(listaSimple.size());

		for(i = 0; existeInicio == false && i <  listaSimple.size(); i++){

			if(formato.format(listaSimple.get(i).getFecha()).equals(formato.format(fechaI))){
				listaPeriodoValido.add(listaSimple.get(i));
				//System.out.println("AGREGA "+listaSimple.get(i).getFecha()+" - "+listaSimple.get(i).getValorInicio());
				//System.out.println("FECHA INICIO MATCH");
				existeInicio = true;
				valorInicio = listaSimple.get(i).getValorInicio();

				for(int y = i+1; existeFin == false && y < listaSimple.size(); y++){
					listaPeriodoValido.add(listaSimple.get(y));
					//System.out.println("AGREGAR 2 "+listaSimple.get(y).getFecha()+" - "+listaSimple.get(y).getValorInicio());
					//System.out.println(listaSimple.get(y).getFecha() + "=="+ fechaF);
					if(formato.format(listaSimple.get(y).getFecha()).equals(formato.format(fechaF))){
						//System.out.println("FECHA FIN MATCH");
						existeFin = true;
						valorFin = listaSimple.get(y).valorFin;
						System.out.println("EXISTE PERIODO");
						return true;
					}
				}
			}
		}

		System.out.println("NO EXISTE PERIODO");
		return false;

	}

	public void calcularVariaciones(ArrayList<Series> lista){//calcula variaciones dentro de un periodo valido

		System.out.println("LISTA VARIACIONES");
		/*for(int h = 0;h< lista.size(); h++){

			System.out.println(lista.get(h).getFecha()+" - "+lista.get(h).getValorInicio()+" - "+lista.get(h).getValorFin()+
					" - "+lista.get(h).getValorMayor()+" - "+lista.get(h).getValorMenor());
		}*/

		int i;
		float porcentaje = Float.valueOf(txtPorcentaje.getText());	
		//listaVariaciones = new ArrayList<Float>();

		for(i = 0; i < lista.size() - 1; i++){

			variacion = ((lista.get(i+1).getValorInicio() - lista.get(i).getValorInicio())*100)/lista.get(i).getValorInicio();
			//System.out.println("FECHA: "+lista.get(i).getFecha()+" - "+lista.get(i+1).getValorInicio()+" - "+lista.get(i).getValorInicio()+" *100 / "+lista.get(i).getValorInicio()+" = "+variacion);
			listaVariaciones.add(variacion);

			if(variacion < porcentaje && !(i == lista.size() - 2)){
				totalVecesPorcentajeMenor += 1;

			}else 
				if(variacion > porcentaje && !(i == lista.size() - 2)){
					totalVecesPorcentajeMayor += 1;

				}
				else if(variacion < porcentaje && (i == lista.size() - 2)){//variacion del final del arreglo
					totalVecesPorcentajeMenorAFechaFin += 1;
				}
				else if(variacion > porcentaje && (i == lista.size() - 2)){
					totalVecesPorcentajeMayorAFechaFin += 1;
				}


			if(variacion > variacionMayor) variacionMayor = variacion;
			if(variacion < variacionMenor) variacionMenor = variacion;

		}


	}

	public boolean existePeriodoVolatilidad(Date fechaI, Date fechaF){

		float valorFin;
		System.out.println("FECHA INICIO V "+fechaI);
		System.out.println("FECHA FIN V "+fechaF);
		boolean existeInicio = false;
		boolean existeFin = false;
		int i;


		for(i = 0; existeInicio == false && i <  listaVolatilidad.size(); i++){

			//System.out.println(listaVolatilidad.get(i).getFecha() + "="+ fechaI);
			if(formato.format(listaVolatilidad.get(i).getFecha()).equals(formato.format(fechaI))){//busca fecha inicio
				//System.out.println("FECHA INICIO V MATCH");
				valorFin = listaVolatilidad.get(i).getValorFin();
				existeInicio = true;

				for(int y = i; existeFin == false && y < listaVolatilidad.size(); y++){//busca fecha fin
					//System.out.println(listaVolatilidad.get(y).getFecha() + "=="+ fechaF);
					//System.out.println(txtMenorQue.getText()+" > "+valorFin+" > "+ txtMayorQue.getText());
					if(formato.format(listaVolatilidad.get(y).getFecha()).equals(formato.format(fechaF))){//comprueba que el valor fin sea correcto
						if(valorFin < Float.valueOf(txtMenorQue.getText()) && valorFin > Float.valueOf(txtMayorQue.getText())){
							//System.out.println("EL VALOR FIN CONCUERDA");
							//System.out.println("FECHA FIN V MATCH");
							existeFin = true;
							//System.out.println("EXISTE PERIODO V");
							return true;
						}
					}
				}
			}
		}

		//System.out.println("NO EXISTE PERIODO V");
		return false;
	}

	public void armarArreglo(float rangoMenor, float rangoMayor){

		System.out.println("RANGO MAYOR "+rangoMayor+" - RANGO MENOR "+rangoMenor);
		Rangos rango = new Rangos();
		listaRangos = new ArrayList<String>();
		listaRangos2 = new ArrayList<Rangos>();
		float numero = (float) 0.25;
		float num = 0;


		while(num <= rangoMayor){

			rango = new Rangos();
			//System.out.println("NUM "+num+" - NUMERO "+ num + numero);
			rango.setDescripcion("ENTRE "+num+" y "+ (num + numero));
			rango.setValorMenor(num);
			rango.setValorMayor(num + numero);
			listaRangos2.add(rango);

			listaRangos.add("ENTRE "+num+" y "+ (num + numero));
			num = num + numero;

		}

		num = 0;

		while(num >= rangoMenor){

			rango = new Rangos();
			rango.setDescripcion("ENTRE "+num+" y "+ (num - numero));
			rango.setValorMenor(num);
			rango.setValorMayor(num - numero);
			listaRangos2.add(rango);


			listaRangos.add("ENTRE "+num+" y "+ (num - numero));
			num = num - numero;

		}

		/*for(Rangos r : listaRangos2){
		    System.out.println(r.getDescripcion()+" - "+r.getValorMenor()+" - "+r.getValorMayor()+" - "+r.getCantidad());
		}*/


		/*for(Float f : listaVariaciones){
			System.out.println(f);
		}*/

		for(Float f : listaVariaciones){// numeros positivos

			for(int i = 0; i < listaRangos2.size(); i++){
				//System.out.println(f+" - "+listaRangos2.get(i).getValorMenor()+" - "+listaRangos2.get(i).getValorMayor());
				if(f > listaRangos2.get(i).getValorMenor() && f < listaRangos2.get(i).getValorMayor()){
					//System.out.println(f+" ENTRE "+listaRangos2.get(i).getValorMenor()+" Y "+listaRangos2.get(i).getValorMayor());

					listaRangos2.get(i).setCantidad(listaRangos2.get(i).getCantidad()+1);
					break;

				}

			}

		}

		for(Float f : listaVariaciones){//numeros negativos

			for(int i = 0; i < listaRangos2.size(); i++){
				if(f < listaRangos2.get(i).getValorMenor() && f > listaRangos2.get(i).getValorMayor()){

					listaRangos2.get(i).setCantidad(listaRangos2.get(i).getCantidad()+1);
					break;

				}

			}

		}

	}



	public void limpiarDatos(){

		txtDias.setText("");
		txtPorcentaje.setText("");
		txtHoraFin.setText("");
		txtHoraInicio.setText("");
		datePickerInicio.getModel().setValue(null);
		datePickerFin.getModel().setValue(null);

		try {
			mascara = new MaskFormatter("##:##");
			txtHoraInicio = new JFormattedTextField(mascara);
			txtHoraFin = new JFormattedTextField(mascara);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}


}
