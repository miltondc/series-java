package Series.Series;

import java.io.Serializable;

public class Rangos implements Serializable{
	
	public String descripcion;
	public float valorMenor;
	public float valorMayor;
	public int cantidad;
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public float getValorMenor() {
		return valorMenor;
	}
	public void setValorMenor(float valorMenor) {
		this.valorMenor = valorMenor;
	}
	public float getValorMayor() {
		return valorMayor;
	}
	public void setValorMayor(float valorMayor) {
		this.valorMayor = valorMayor;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	

}
