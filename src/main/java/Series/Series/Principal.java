package Series.Series;

import java.awt.EventQueue;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;


public class Principal extends JFrame{
	
	PanelPrincipal panelPrincipal;
	TimeZone tz = null;

	public Principal() {
		getContentPane().setLayout(null);
		setBounds(50, 50, 1013, 679);
		
		panelPrincipal = new PanelPrincipal();
		panelPrincipal.setBounds(34, 11, 924, 625);
		getContentPane().add(panelPrincipal);
		
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
								
				try {
					boolean b = false;
					for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
						if ("Nimbus".equals(info.getName())) {
							UIManager.setLookAndFeel(info.getClassName());
							b = true;
							break;
						}
					}
					if (!b)	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					Principal frame = new Principal();
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
															
					frame.setVisible(true);
					

				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
