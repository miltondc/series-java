package Series.Series;

import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

public class VentanaResultado extends JDialog{
	
	JLabel lblTotalPeriodosValidos;
	JLabel lblTotalPeriodosQue;
	JLabel lblRangoVariacionMayor;
	JLabel lblRangoVariacionMenor;
	JLabel lblRangoVariacionMayor_1;
	JLabel lblRangoVariacionMenor_1;
	JLabel varPeriodosValidos;
	JLabel varPeriodosVolatilidad;
	JLabel varVariacionMayor;
	JLabel varVariacionMenor;
	JLabel varVarMayorFF;
	JLabel varVarMenorFF;
	
	JScrollPane panelVariaciones;
	DefaultTableModel modeloVariaciones;
	JTable tablaVariaciones;
	private JPanel panel;
		
	
	public VentanaResultado(String porcentaje) {
		
		getContentPane().setLayout(null);
		
		setBounds(200, 50, 800, 500);
		setModal(true);
				
		panelVariaciones = new JScrollPane();
		panelVariaciones.setBounds(470, 37, 283, 334);
		getContentPane().add(panelVariaciones);
		
		modeloVariaciones = new DefaultTableModel();
		modeloVariaciones.addColumn("RANGO");
		modeloVariaciones.addColumn("CANTIDAD");
		
		tablaVariaciones = new JTable(modeloVariaciones){
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		
		tablaVariaciones.getColumnModel().getColumn(0).setMaxWidth(200);
		tablaVariaciones.getColumnModel().getColumn(0).setMinWidth(200);
		tablaVariaciones.getColumnModel().getColumn(0).setPreferredWidth(200);

		tablaVariaciones.getColumnModel().getColumn(1).setMaxWidth(80);
		tablaVariaciones.getColumnModel().getColumn(1).setMinWidth(80);
		tablaVariaciones.getColumnModel().getColumn(1).setPreferredWidth(80);
		
		panelVariaciones.add(tablaVariaciones);
		panelVariaciones.setViewportView(tablaVariaciones);
		getContentPane().add(panelVariaciones);
		
		panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(41, 37, 382, 334);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		
		
		
		lblTotalPeriodosValidos = new JLabel("Periodos Validos:");
		lblTotalPeriodosValidos.setBounds(10, 21, 213, 23);
		panel.add(lblTotalPeriodosValidos);
		
		lblTotalPeriodosQue = new JLabel("Periodos Volatilidad");
		lblTotalPeriodosQue.setBounds(10, 55, 213, 23);
		panel.add(lblTotalPeriodosQue);
		
		lblRangoVariacionMayor = new JLabel("Rango Variacion Mayor que "+porcentaje);
		lblRangoVariacionMayor.setBounds(10, 104, 236, 23);
		panel.add(lblRangoVariacionMayor);
		
		lblRangoVariacionMenor = new JLabel("Rango Variacion Menor que "+porcentaje);
		lblRangoVariacionMenor.setBounds(10, 153, 236, 23);
		panel.add(lblRangoVariacionMenor);
		
		lblRangoVariacionMayor_1 = new JLabel("Rango Variacion Mayor a Fecha Fin");
		lblRangoVariacionMayor_1.setBounds(10, 208, 213, 14);
		panel.add(lblRangoVariacionMayor_1);
		
		lblRangoVariacionMenor_1 = new JLabel("Rango Variacion Menor a Fecha Fin");
		lblRangoVariacionMenor_1.setBounds(10, 256, 213, 14);
		panel.add(lblRangoVariacionMenor_1);
		
		varPeriodosValidos = new JLabel("Q");
		varPeriodosValidos.setBounds(275, 20, 80, 25);
		panel.add(varPeriodosValidos);
		
		varPeriodosVolatilidad = new JLabel("Q");
		varPeriodosVolatilidad.setBounds(275, 54, 80, 25);
		panel.add(varPeriodosVolatilidad);
		
		varVariacionMayor = new JLabel("Q");
		varVariacionMayor.setBounds(275, 103, 80, 25);
		panel.add(varVariacionMayor);
		
		varVarMenorFF = new JLabel("Q");
		varVarMenorFF.setBounds(275, 251, 80, 25);
		panel.add(varVarMenorFF);
		
		varVariacionMenor = new JLabel("Q");
		varVariacionMenor.setBounds(275, 152, 80, 25);
		panel.add(varVariacionMenor);
		
		varVarMayorFF = new JLabel("Q");
		varVarMayorFF.setBounds(275, 203, 80, 25);
		panel.add(varVarMayorFF);
		
		
	}
	
	
	public void completarDatos(ArrayList<Rangos> listaVar,int totalPeriodosValidos, int totalPeriodosVolatilidad,
			int totalVariacionMayor,int totalVariacionMenor, int totalVarMayorFF, int totalVarMenorFF){
		
		varPeriodosValidos.setText(String.valueOf(totalPeriodosValidos));
		varPeriodosVolatilidad.setText(String.valueOf(totalPeriodosVolatilidad));
		varVariacionMayor.setText(String.valueOf(totalVariacionMayor));
		varVariacionMenor.setText(String.valueOf(totalVariacionMenor));
		varVarMayorFF.setText(String.valueOf(totalVarMayorFF));
		varVarMenorFF.setText(String.valueOf(totalVarMenorFF));
		
		for(Rangos r : listaVar){
			
			Object [] o = new Object[2];
			
			o[0] = r.getDescripcion();
			o[1] = r.getCantidad();
			
			modeloVariaciones.addRow(o);
			
		}
		
		
	}
}
